package se.lublin.stc.service.stc.response;

import java.io.Serializable;

public class Response<T> implements Serializable {
    private T data;

    public T getData(){
        return data;
    }
}