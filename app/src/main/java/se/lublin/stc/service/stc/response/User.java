package se.lublin.stc.service.stc.response;

public class User {
    private Integer unumber;
    private String name;
    private String desa;
    private String kelompok;
    private String phone;
    private Integer verified;
    private String updated_at;
    private String created_at;

    @Override
    public String toString() {
        return "User{" +
                ", unumber=" + unumber +
                ", name='" + name + '\'' +
                ", kelompok='" + kelompok + '\'' +
                ", phone='" + phone + '\'' +
                ", verified=" + verified +
                ", updated_at='" + updated_at + '\'' +
                ", created_at='" + created_at + '\'' +
                '}';
    }

    public String getDesa() {
        return desa;
    }

    public Integer getVerified() {
        return verified;
    }


    public Integer getUnumber() {
        return unumber;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getName() {
        return name;
    }

    public String getKelompok() {
        return kelompok;
    }

    public String getPhone() {
        return phone;
    }
}
