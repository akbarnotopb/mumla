package se.lublin.stc.service.stc.api;

public class ServiceApi {
    private static final String BASE_WEB     = "https://stc.akfisa.com/";
//    private static final String BASE_WEB     = "https://sstc.shelterapp.co.id/";
    private static final String BASE_URL_API = BASE_WEB+"api/";

    public static ServiceInterface getApiService() {
        return ServiceGenerator.getClient(BASE_URL_API).create(ServiceInterface.class);
    }

    public static String getBaseUrl(){
        return BASE_WEB;
    }
}
