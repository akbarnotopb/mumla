package se.lublin.stc.service.stc.response;

public class Server {
    private int id;
    private String name;
    private String address;
    private Integer port;
    private String password;
    private Integer active;

    public Integer getActive(){
        return active;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public Integer getPort() {
        return port;
    }

    public String getPassword() {
        return password;
    }
}
