package se.lublin.stc.service.stc.api;

import java.util.Map;


import se.lublin.stc.service.stc.response.Response;
import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import se.lublin.stc.service.stc.response.Result;

public interface ServiceInterface {

    @FormUrlEncoded
    @POST("register")
    Observable<Response<Result>> register(@Header("Authorization") String authHeader, @FieldMap Map<String, String> field);

    @GET("server")
    Observable<Response<Result>> server(@Header("Authorization") String authHeader);

}
