package se.lublin.stc.service.stc.response;

import java.util.List;

public class Result<T> {
    private String token;
    private User user;
    private List<Server> server;

    public List<Server> getServer(){
        return server;
    }

    public String getToken(){
        return token;
    }

    public User getUser(){
        return user;
    }

    @Override
    public String toString() {
        return "Result{" +
                "token='" + token + '\'' +
                ", user=" + user +
                ", server=" + server +
                '}';
    }
}
