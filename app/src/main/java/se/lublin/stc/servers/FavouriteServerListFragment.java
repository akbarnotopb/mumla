/*
 * Copyright (C) 2014 Andrew Comminos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package se.lublin.stc.servers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import java.util.List;

import se.lublin.humla.model.Server;
import se.lublin.stc.R;
import se.lublin.stc.db.DatabaseProvider;
import se.lublin.stc.db.PublicServer;
import se.lublin.stc.preference.account.AccountSettingFragment;
import se.lublin.stc.servers.stcserver.stcServerViewModel;

/**
 * Displays a list of servers, and allows the user to connect and edit them.
 * @author morlunk
 *
 */
public class FavouriteServerListFragment extends Fragment implements OnItemClickListener, FavouriteServerAdapter.FavouriteServerAdapterMenuListener {

    private ServerConnectHandler mConnectHandler;
    private DatabaseProvider mDatabaseProvider;
    private GridView mServerGrid;
    private ServerAdapter<Server> mServerAdapter;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private Context context;
    private stcServerViewModel viewModel;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mConnectHandler = (ServerConnectHandler)activity;
            mDatabaseProvider = (DatabaseProvider) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()+" must implement ServerConnectHandler!");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_server_list, container, false);

        viewModel = new stcServerViewModel(getActivity().getApplicationContext());
        //progressDialog = new ProgressDialog(getActivity());
        //progressDialog.setMessage("Sedang mengambil data...");
        //progressDialog.show();
        subscribeToViewModel();

        mServerGrid = (GridView) view.findViewById(R.id.server_list_grid);
        mServerGrid.setOnItemClickListener(this);
        mServerGrid.setEmptyView(view.findViewById(R.id.server_list_grid_empty));



        sharedPreferences = getActivity().getApplicationContext().getSharedPreferences("preferences",0);
        editor = sharedPreferences.edit();

        viewModel.server();

        List<Server> servers = mDatabaseProvider.getDatabase().getServers();
        for (int i =0; i < servers.size(); ++i){
            mDatabaseProvider.getDatabase().removeServer(servers.get(i));
        }

        registerForContextMenu(mServerGrid);
        return view;
    }

    public static String toCamelCase(String s){
        if(s.length() == 0){
            return s;
        }
        String[] parts = s.split(" ");
        String camelCaseString = "";
        for (String part : parts){
            camelCaseString = camelCaseString + toProperCase(part) + "";
        }
        return camelCaseString;
    }

    public static String toProperCase(String s) {
        return s.substring(0, 1).toUpperCase() +
                s.substring(1).toLowerCase();
    }

    private void subscribeToViewModel(){
        Log.i("apifetching","i'm waiting");
        viewModel.getResult().observe(this, result->{
            String unumber = result.getUser().getUnumber()+"";

            if(result.getUser().getUnumber() < 10){
                unumber = "00"+result.getUser().getUnumber()+"";
            }else if(result.getUser().getUnumber() <100){
                unumber = "0"+result.getUser().getUnumber()+"";
            }

            editor.putBoolean("PREFERENCE_VERIFIED", false);
            editor.putInt("PREFERENCE_UNUMBER", result.getUser().getUnumber());
            if(result.getUser().getVerified() == 1){
                editor.putBoolean("PREFERENCE_VERIFIED", true);
            }
            editor.commit();

            String username = "16.Jatim_02_"+toCamelCase(result.getUser().getDesa())+"_"+toCamelCase(result.getUser().getKelompok())+"."+unumber+"_"+toCamelCase(result.getUser().getName())+"_"+result.getUser().getPhone();

            List<se.lublin.stc.service.stc.response.Server> servers = result.getServer();
            for (int i = 0 ; i < servers.size(); ++i){
                se.lublin.stc.service.stc.response.Server responseServer = servers.get(i);
                Server server = new Server(responseServer.getId(), responseServer.getName() , responseServer.getAddress(), responseServer.getPort(), username, responseServer.getPassword());
                mDatabaseProvider.getDatabase().addServer(server);
            }
            //progressDialog.dismiss();
            updateServers();
            Toast.makeText(getActivity(), "Berhasil mengambil data...", Toast.LENGTH_SHORT).show();
        });

        viewModel.getError().observe(this, result->{
            //progressDialog.dismiss();
            Class<? extends Fragment> fragmentClass = null;
            Bundle args = new Bundle();
            fragmentClass = AccountSettingFragment.class;
            Fragment fragment = Fragment.instantiate(getActivity(), fragmentClass.getName(), args);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, fragment, fragmentClass.getName())
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
            getActivity().setTitle("Akun");
            Toast.makeText(getActivity().getApplicationContext(),"Akun Anda dinonaktifkan pengurus / Sesi Anda habis, amshol simpan ulang akun Anda!",Toast.LENGTH_LONG).show();
        });


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_server_list, menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateServers();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.menu_add_server_item:
//                addServer();
//                return true;
//            case R.id.menu_quick_connect:
//                ServerEditFragment.createServerEditDialog(getActivity(), null, ServerEditFragment.Action.CONNECT_ACTION, true)
//                        .show(getFragmentManager(), "serverInfo");
//                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void addServer() {
        ServerEditFragment.createServerEditDialog(getActivity(), null, ServerEditFragment.Action.ADD_ACTION, false)
                .show(getFragmentManager(), "serverInfo");
    }

    public void editServer(Server server) {
        ServerEditFragment.createServerEditDialog(getActivity(), server, ServerEditFragment.Action.EDIT_ACTION, false)
                .show(getFragmentManager(), "serverInfo");
    }

    public void shareServer(Server server) {
        // Build Mumble server URL
        String serverUrl = "mumble://" + server.getHost()
            + (server.getPort() == 0 ? "" : ":" + server.getPort()) + "/";

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.shareMessage, serverUrl));
        intent.setType("text/plain");
        startActivity(intent);
    }

    public void deleteServer(final Server server) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        alertBuilder.setMessage(R.string.confirm_delete_server);
        alertBuilder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDatabaseProvider.getDatabase().removeServer(server);
                mServerAdapter.remove(server);
            }
        });
        alertBuilder.setNegativeButton(android.R.string.cancel, null);
        alertBuilder.show();
    }

    public void updateServers() {
        List<Server> servers = getServers();
        mServerAdapter = new FavouriteServerAdapter(getActivity(), servers);
        mServerGrid.setAdapter(mServerAdapter);
    }



    public List<Server> getServers() {
        List<Server> servers = mDatabaseProvider.getDatabase().getServers();
        return servers;
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        mConnectHandler.connectToServer(mServerAdapter.getItem(arg2));
    }

    public static interface ServerConnectHandler {
        public void connectToServer(Server server);
        public void connectToPublicServer(PublicServer server);
    }
}
