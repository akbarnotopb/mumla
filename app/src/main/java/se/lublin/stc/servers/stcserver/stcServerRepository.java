package se.lublin.stc.servers.stcserver;

import android.content.Context;
import android.util.Log;

import io.reactivex.Observable;
import se.lublin.stc.service.stc.api.ServiceApi;
import se.lublin.stc.service.stc.api.ServiceInterface;
import se.lublin.stc.service.stc.response.Response;
import se.lublin.stc.service.stc.response.Result;

public class stcServerRepository {
    private ServiceInterface serviceInterface;

    public stcServerRepository() {
        serviceInterface = ServiceApi.getApiService();
    }

    public Observable<Response<Result>> server(Context context){
        String token = context.getSharedPreferences("preferences",0).getString("PREFERENCE_TOKEN_STC","");
        return serviceInterface.server( "Bearer " + token);
    }
}
