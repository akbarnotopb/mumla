package se.lublin.stc.servers.stcserver;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.io.IOException;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import se.lublin.stc.service.stc.response.Response;
import se.lublin.stc.service.stc.response.Result;

public class stcServerViewModel {
    private stcServerRepository repository;
    private MutableLiveData<Result> result;
    private MutableLiveData<Boolean> error;
    private Context context;

    public stcServerViewModel(Context context){
        repository      = new stcServerRepository();
        result          = new MutableLiveData<Result>();
        error           = new MutableLiveData<>();
        this.context    = context;
    }

    private void requestFailed(Throwable throwable){
        if(throwable instanceof IOException){
            error.setValue(false);
        }else{
            error.setValue(false);
        }
    }

    private void requestSuccess(Response<Result> response){
        result.setValue(response.getData());
    }

    public void server(){
        Log.i("apifetching","request sent");
        repository.server(context)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::requestSuccess, this::requestFailed);
    }
    public MutableLiveData<Result> getResult() {
        return result;
    }

    public MutableLiveData<Boolean> getError() {
        return error;
    }
}
