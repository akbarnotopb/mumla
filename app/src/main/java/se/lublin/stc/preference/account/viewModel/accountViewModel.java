package se.lublin.stc.preference.account.viewModel;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import se.lublin.stc.preference.account.repository.accountRepository;
import se.lublin.stc.service.stc.response.Response;
import se.lublin.stc.service.stc.response.Result;

public class accountViewModel {

    private accountRepository repository;
    private MutableLiveData<Result> result;
    private MutableLiveData<Boolean> error;
    private Context context;

    public accountViewModel(Context context){
        repository      = new accountRepository();
        result          = new MutableLiveData<Result>();
        error           = new MutableLiveData<>();
        this.context    = context;
    }

    private void requestFailed(Throwable throwable){
        Log.d("apierror", throwable.getMessage());
        throwable.printStackTrace();
        if(throwable instanceof IOException){
            error.setValue(false);
        }else{
            error.setValue(false);
        }
    }

    private void requestSuccess(Response<Result> response){
        result.setValue(response.getData());
    }

    public void register(String name, String phone, String kelompok){


        Map<String, String> requestBody = new HashMap<String, String>();
        requestBody.put("name", name);
        requestBody.put("phone", phone);
        requestBody.put("kelompok", kelompok);

        repository.store(requestBody, this.context)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::requestSuccess, this::requestFailed);
    }
    public MutableLiveData<Result> getResult() {
        return result;
    }


    public MutableLiveData<Boolean> getError() {
        return error;
    }

    public void resetResult() {
        result.setValue(null);
    }

    public void resetError() {
        error.setValue(null);
    }
}
