package se.lublin.stc.preference.account;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import se.lublin.stc.R;
import se.lublin.stc.preference.account.viewModel.accountViewModel;
import se.lublin.stc.servers.FavouriteServerListFragment;

public class AccountSettingFragment extends Fragment {
    Spinner select;
    Button btn_kirim;
    EditText et_nama;
    EditText et_nohp;
    TextView tv_verifiedser;
    TextView tv_nourut;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private Context context;
    private accountViewModel viewModel;
    ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedBundleInstance){
        super.onCreate(savedBundleInstance);
        setHasOptionsMenu(true);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Sedang menyimpan data!");
        viewModel = new accountViewModel(getActivity().getApplicationContext());
        subscribeToViewModel();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account_setting, container, false);
        select = view.findViewById(R.id.select_kelompok);
        String[] items = new String[]{"Pilih Kelompok", "Keputih", "Semampir","Praja","Nginden Utara", "Nginden Selatan", "Nginden Timur", "Ngagel Mulyo","Ngagel Rejo","Krukah", "Gubeng", "PPM KH1", "PPM KH2", "PPM KH3"};
        ArrayAdapter<String> options = new ArrayAdapter<>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, items);
        btn_kirim = view.findViewById(R.id.btn_kirim_biodata);
        sharedPreferences = getActivity().getApplicationContext().getSharedPreferences("preferences",0);
        editor = sharedPreferences.edit();

        et_nama = view.findViewById(R.id.et_name);
        et_nohp = view.findViewById(R.id.et_phoneNumber);
        tv_nourut   = view.findViewById(R.id.tv_nourut);
        tv_verifiedser  = view.findViewById(R.id.tv_verifiedUser);

        select.setAdapter(options);

        String name = sharedPreferences.getString("PREFERENCE_NAME","");
        String nohp = sharedPreferences.getString("PREFERENCE_NOHP", "");
        Integer pos = sharedPreferences.getInt("PREFERENCE_KIDX",0);
        Boolean verified = sharedPreferences.getBoolean("PREFERENCE_VERIFIED",false);
        Integer unumber  = sharedPreferences.getInt("PREFERENCE_UNUMBER",1);

        if(verified){
            tv_verifiedser.setText("Telah Diverifikasi");
            tv_nourut.setText("No Urut : " +unumber.toString());
        }


        if(name != null && name.length() != 0){
            et_nama.setText(name);
        }
        if(nohp != null && nohp.length() !=0){
            et_nohp.setText(nohp);
        }

        select.setSelection(pos);

        btn_kirim.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(et_nama.getText().toString().length() == 0 || et_nama.getText().toString() == ""){
                    Toast.makeText(getActivity().getApplicationContext(),"Nama Tidak Boleh Kosong!", Toast.LENGTH_LONG).show();
                    return;
                }
                else if(select.getSelectedItemPosition() == 0){
                    Toast.makeText(getActivity().getApplicationContext(), "Pilihlah kelompok!", Toast.LENGTH_LONG).show();
                    return;
                }
                else if(et_nohp.getText().toString().length() == 0 || et_nohp.getText().toString() == ""){
                    Toast.makeText(getActivity().getApplicationContext(), "No HP tidak boleh kosong!", Toast.LENGTH_LONG).show();
                    return;
                }

                String nama = et_nama.getText().toString();
                String hp   = et_nohp.getText().toString();
                String kelp = select.getSelectedItem().toString();
                Integer pos = select.getSelectedItemPosition();

                editor.putString("PREFERENCE_NAME", nama);
                editor.putString("PREFERENCE_NOHP", hp);
                editor.putString("PREFERENCE_KELP", kelp);
                editor.putInt("PREFERENCE_KIDX", pos);
                editor.commit();
                progressDialog.show();
                viewModel.register(nama,hp,kelp);
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_server_list, menu);
    }

    private void subscribeToViewModel(){
        viewModel.getResult().observe(this, result->{
            if(result != null){
                String verified = "Belum Diverifikasi";
                editor.putBoolean("PREFERENCE_VERIFIED", false);
                editor.putInt("PREFERENCE_UNUMBER", result.getUser().getUnumber());

                if(result.getUser().getVerified() == 1){
                    verified = "Telah Diverifikasi!";
                    editor.putBoolean("PREFERENCE_VERIFIED", true);

                    tv_nourut.setText("No Urut : "+result.getUser().getUnumber());
                    tv_verifiedser.setText(verified);
                }

                editor.putString("PREFERENCE_TOKEN_STC", result.getToken());
                editor.commit();

                progressDialog.dismiss();
                viewModel.resetResult();

                Class<? extends Fragment> fragmentClass = null;
                Bundle args = new Bundle();
                fragmentClass = FavouriteServerListFragment.class;
                Fragment fragment = Fragment.instantiate(getActivity(), fragmentClass.getName(), args);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, fragment, fragmentClass.getName())
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                getActivity().setTitle("Server Suteng");

                Toast.makeText(getActivity().getApplicationContext(),"Sukses menyimpan Data! Silahkan join server",Toast.LENGTH_LONG).show();
            }
        });

        viewModel.getError().observe(this, result->{
            if(result != null){
                progressDialog.dismiss();
                viewModel.resetError();
                Toast.makeText(getActivity().getApplicationContext(),"Terjadi Kesalahan, Cobalah beberapa saat lagi!",Toast.LENGTH_LONG).show();
            }
        });
    }
}

