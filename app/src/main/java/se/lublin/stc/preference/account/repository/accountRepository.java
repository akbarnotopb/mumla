package se.lublin.stc.preference.account.repository;

import android.content.Context;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.PartMap;
import se.lublin.stc.service.stc.api.ServiceApi;
import se.lublin.stc.service.stc.api.ServiceInterface;
import se.lublin.stc.service.stc.response.Response;
import se.lublin.stc.service.stc.response.Result;

public class accountRepository {
    private ServiceInterface serviceInterface;

    public accountRepository() {
        serviceInterface = ServiceApi.getApiService();
    }

    public Observable<Response<Result>> store(@PartMap Map<String, String> data, Context context){
        String token = context.getSharedPreferences("preferences",0).getString("PREFERENCE_TOKEN_STC","");
        return serviceInterface.register( "Bearer " + token, data);
    }
}
